/***********************************/

//	 GLOBAL

/***********************************/

var userAgent = navigator.userAgent || navigator.vendor || window.opera;
var elHtml = document.getElementsByTagName('html')[0];

detectBrowser =  {
	init:function() {
		this.ieVersion();
		this.detectMobile();
	},

	ieVersion:function(){
        var msie = userAgent.indexOf('MSIE ');
        var trident = userAgent.indexOf('Trident/');
        var edge = userAgent.indexOf('Edge/');
        if (trident > 0) {
            // IE 11 
           elHtml.className += ' ie11'


        }
        else if (edge > 0) {
            // Edge 
           elHtml.className += ' ieEdge'

        }
        else{
           elHtml.className += ' notIE'

        }
	},

	detectMobile: function() {
	  	// Windows Phone must come first because its UA also contains "Android"
	    if (/windows phone/i.test(userAgent)) {
	        elHtml.className +=  ' winPhone';
	    }

	    else if (/android/i.test(userAgent)) {
	        elHtml.className +=  ' android';
	    }

	    // iOS detection from: http://stackoverflow.com/a/9039885/177710
	    else if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
	        elHtml.className +=  ' iOS';
	    }

	    else {
	    	elHtml.className +=  ' notMobile';
	    }
	}
};

detectBrowser.init();


/***********************************/
//
//	 Jquery Vide
//
/***********************************/
var videoTextos =  {
    "lego":{
    	"titulo": "lego titulo",
    	"subTitulo": "lego subTitulo",
    	"texto": "lego texto",
    	"link1": "http://www.link.com",
    	"img1": "img1"
    },
   "ocean":{
    	"titulo": "ocean titulo",
    	"subTitulo": "ocean subTitulo",
    	"texto": "ocean texto"
    },
};


videoBox = {
	init: function() {

		this.mudaVideo('lego');
		var $botoes = $('.btns-videos button');

		$botoes.on('click', function(){
			var $this = $(this),
				thisVid = $this.attr('data-video');

			videoBox.botaoClick(thisVid, $botoes, $(this))
		})
	},

	botaoClick: function(videoName, outros, elemento) {
		this.mudaVideo(videoName);
		outros.attr('disabled', false);
		elemento.attr('disabled', true);
	},

	mudaVideo: function(videoName) {
		$('aside.overlay').fadeIn(200);
		setTimeout(function(){
			$('#myBlock').vide({
		  		mp4: 'files/video/' + videoName
			}, {
				loop: true, 
				muted: true, 
			 	autoplay: true,
			 	posterType: "none",
				position: "50% 50%"
			});

			$('#videoTxt').html(
				'<h2>' + videoTextos[videoName]["titulo"] + '</h2>' +
				'<h3>' + videoTextos[videoName]["subTitulo"] + '</h3>' +
				'<p>' + videoTextos[videoName]["texto"] + '</p>' 
			);

		}, 250)

		$('aside.overlay').delay(300).fadeOut();

	}

	// mudaTexto: function()

}

videoBox.init();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdsb2JhbC5qcyIsInZpZGVvR2FsbGVyeS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMzREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8vXHQgR0xPQkFMXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxudmFyIHVzZXJBZ2VudCA9IG5hdmlnYXRvci51c2VyQWdlbnQgfHwgbmF2aWdhdG9yLnZlbmRvciB8fCB3aW5kb3cub3BlcmE7XG52YXIgZWxIdG1sID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2h0bWwnKVswXTtcblxuZGV0ZWN0QnJvd3NlciA9ICB7XG5cdGluaXQ6ZnVuY3Rpb24oKSB7XG5cdFx0dGhpcy5pZVZlcnNpb24oKTtcblx0XHR0aGlzLmRldGVjdE1vYmlsZSgpO1xuXHR9LFxuXG5cdGllVmVyc2lvbjpmdW5jdGlvbigpe1xuICAgICAgICB2YXIgbXNpZSA9IHVzZXJBZ2VudC5pbmRleE9mKCdNU0lFICcpO1xuICAgICAgICB2YXIgdHJpZGVudCA9IHVzZXJBZ2VudC5pbmRleE9mKCdUcmlkZW50LycpO1xuICAgICAgICB2YXIgZWRnZSA9IHVzZXJBZ2VudC5pbmRleE9mKCdFZGdlLycpO1xuICAgICAgICBpZiAodHJpZGVudCA+IDApIHtcbiAgICAgICAgICAgIC8vIElFIDExIFxuICAgICAgICAgICBlbEh0bWwuY2xhc3NOYW1lICs9ICcgaWUxMSdcblxuXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoZWRnZSA+IDApIHtcbiAgICAgICAgICAgIC8vIEVkZ2UgXG4gICAgICAgICAgIGVsSHRtbC5jbGFzc05hbWUgKz0gJyBpZUVkZ2UnXG5cbiAgICAgICAgfVxuICAgICAgICBlbHNle1xuICAgICAgICAgICBlbEh0bWwuY2xhc3NOYW1lICs9ICcgbm90SUUnXG5cbiAgICAgICAgfVxuXHR9LFxuXG5cdGRldGVjdE1vYmlsZTogZnVuY3Rpb24oKSB7XG5cdCAgXHQvLyBXaW5kb3dzIFBob25lIG11c3QgY29tZSBmaXJzdCBiZWNhdXNlIGl0cyBVQSBhbHNvIGNvbnRhaW5zIFwiQW5kcm9pZFwiXG5cdCAgICBpZiAoL3dpbmRvd3MgcGhvbmUvaS50ZXN0KHVzZXJBZ2VudCkpIHtcblx0ICAgICAgICBlbEh0bWwuY2xhc3NOYW1lICs9ICAnIHdpblBob25lJztcblx0ICAgIH1cblxuXHQgICAgZWxzZSBpZiAoL2FuZHJvaWQvaS50ZXN0KHVzZXJBZ2VudCkpIHtcblx0ICAgICAgICBlbEh0bWwuY2xhc3NOYW1lICs9ICAnIGFuZHJvaWQnO1xuXHQgICAgfVxuXG5cdCAgICAvLyBpT1MgZGV0ZWN0aW9uIGZyb206IGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9hLzkwMzk4ODUvMTc3NzEwXG5cdCAgICBlbHNlIGlmICgvaVBhZHxpUGhvbmV8aVBvZC8udGVzdCh1c2VyQWdlbnQpICYmICF3aW5kb3cuTVNTdHJlYW0pIHtcblx0ICAgICAgICBlbEh0bWwuY2xhc3NOYW1lICs9ICAnIGlPUyc7XG5cdCAgICB9XG5cblx0ICAgIGVsc2Uge1xuXHQgICAgXHRlbEh0bWwuY2xhc3NOYW1lICs9ICAnIG5vdE1vYmlsZSc7XG5cdCAgICB9XG5cdH1cbn07XG5cbmRldGVjdEJyb3dzZXIuaW5pdCgpO1xuXG4iLCIvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4vL1xuLy9cdCBKcXVlcnkgVmlkZVxuLy9cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbnZhciB2aWRlb1RleHRvcyA9ICB7XG4gICAgXCJsZWdvXCI6e1xuICAgIFx0XCJ0aXR1bG9cIjogXCJsZWdvIHRpdHVsb1wiLFxuICAgIFx0XCJzdWJUaXR1bG9cIjogXCJsZWdvIHN1YlRpdHVsb1wiLFxuICAgIFx0XCJ0ZXh0b1wiOiBcImxlZ28gdGV4dG9cIixcbiAgICBcdFwibGluazFcIjogXCJodHRwOi8vd3d3LmxpbmsuY29tXCIsXG4gICAgXHRcImltZzFcIjogXCJpbWcxXCJcbiAgICB9LFxuICAgXCJvY2VhblwiOntcbiAgICBcdFwidGl0dWxvXCI6IFwib2NlYW4gdGl0dWxvXCIsXG4gICAgXHRcInN1YlRpdHVsb1wiOiBcIm9jZWFuIHN1YlRpdHVsb1wiLFxuICAgIFx0XCJ0ZXh0b1wiOiBcIm9jZWFuIHRleHRvXCJcbiAgICB9LFxufTtcblxuXG52aWRlb0JveCA9IHtcblx0aW5pdDogZnVuY3Rpb24oKSB7XG5cblx0XHR0aGlzLm11ZGFWaWRlbygnbGVnbycpO1xuXHRcdHZhciAkYm90b2VzID0gJCgnLmJ0bnMtdmlkZW9zIGJ1dHRvbicpO1xuXG5cdFx0JGJvdG9lcy5vbignY2xpY2snLCBmdW5jdGlvbigpe1xuXHRcdFx0dmFyICR0aGlzID0gJCh0aGlzKSxcblx0XHRcdFx0dGhpc1ZpZCA9ICR0aGlzLmF0dHIoJ2RhdGEtdmlkZW8nKTtcblxuXHRcdFx0dmlkZW9Cb3guYm90YW9DbGljayh0aGlzVmlkLCAkYm90b2VzLCAkKHRoaXMpKVxuXHRcdH0pXG5cdH0sXG5cblx0Ym90YW9DbGljazogZnVuY3Rpb24odmlkZW9OYW1lLCBvdXRyb3MsIGVsZW1lbnRvKSB7XG5cdFx0dGhpcy5tdWRhVmlkZW8odmlkZW9OYW1lKTtcblx0XHRvdXRyb3MuYXR0cignZGlzYWJsZWQnLCBmYWxzZSk7XG5cdFx0ZWxlbWVudG8uYXR0cignZGlzYWJsZWQnLCB0cnVlKTtcblx0fSxcblxuXHRtdWRhVmlkZW86IGZ1bmN0aW9uKHZpZGVvTmFtZSkge1xuXHRcdCQoJ2FzaWRlLm92ZXJsYXknKS5mYWRlSW4oMjAwKTtcblx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG5cdFx0XHQkKCcjbXlCbG9jaycpLnZpZGUoe1xuXHRcdCAgXHRcdG1wNDogJ2ZpbGVzL3ZpZGVvLycgKyB2aWRlb05hbWVcblx0XHRcdH0sIHtcblx0XHRcdFx0bG9vcDogdHJ1ZSwgXG5cdFx0XHRcdG11dGVkOiB0cnVlLCBcblx0XHRcdCBcdGF1dG9wbGF5OiB0cnVlLFxuXHRcdFx0IFx0cG9zdGVyVHlwZTogXCJub25lXCIsXG5cdFx0XHRcdHBvc2l0aW9uOiBcIjUwJSA1MCVcIlxuXHRcdFx0fSk7XG5cblx0XHRcdCQoJyN2aWRlb1R4dCcpLmh0bWwoXG5cdFx0XHRcdCc8aDI+JyArIHZpZGVvVGV4dG9zW3ZpZGVvTmFtZV1bXCJ0aXR1bG9cIl0gKyAnPC9oMj4nICtcblx0XHRcdFx0JzxoMz4nICsgdmlkZW9UZXh0b3NbdmlkZW9OYW1lXVtcInN1YlRpdHVsb1wiXSArICc8L2gzPicgK1xuXHRcdFx0XHQnPHA+JyArIHZpZGVvVGV4dG9zW3ZpZGVvTmFtZV1bXCJ0ZXh0b1wiXSArICc8L3A+JyBcblx0XHRcdCk7XG5cblx0XHR9LCAyNTApXG5cblx0XHQkKCdhc2lkZS5vdmVybGF5JykuZGVsYXkoMzAwKS5mYWRlT3V0KCk7XG5cblx0fVxuXG5cdC8vIG11ZGFUZXh0bzogZnVuY3Rpb24oKVxuXG59XG5cbnZpZGVvQm94LmluaXQoKTsiXX0=
