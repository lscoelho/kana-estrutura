/***********************************/

//	 GLOBAL

/***********************************/

var userAgent = navigator.userAgent || navigator.vendor || window.opera;
var elHtml = document.getElementsByTagName('html')[0];

detectBrowser =  {
	init:function() {
		this.ieVersion();
		this.detectMobile();
	},

	ieVersion:function(){
        var msie = userAgent.indexOf('MSIE ');
        var trident = userAgent.indexOf('Trident/');
        var edge = userAgent.indexOf('Edge/');
        if (trident > 0) {
            // IE 11 
           elHtml.className += ' ie11'


        }
        else if (edge > 0) {
            // Edge 
           elHtml.className += ' ieEdge'

        }
        else{
           elHtml.className += ' notIE'

        }
	},

	detectMobile: function() {
	  	// Windows Phone must come first because its UA also contains "Android"
	    if (/windows phone/i.test(userAgent)) {
	        elHtml.className +=  ' winPhone';
	    }

	    else if (/android/i.test(userAgent)) {
	        elHtml.className +=  ' android';
	    }

	    // iOS detection from: http://stackoverflow.com/a/9039885/177710
	    else if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
	        elHtml.className +=  ' iOS';
	    }

	    else {
	    	elHtml.className +=  ' notMobile';
	    }
	}
};

detectBrowser.init();

