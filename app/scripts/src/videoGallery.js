/***********************************/
//
//	 Jquery Vide
//
/***********************************/
var videoTextos =  {
    "lego":{
    	"titulo": "lego titulo",
    	"subTitulo": "lego subTitulo",
    	"texto": "lego texto",
    	"link1": "http://www.link.com",
    	"img1": "img1"
    },
   "ocean":{
    	"titulo": "ocean titulo",
    	"subTitulo": "ocean subTitulo",
    	"texto": "ocean texto"
    },
};


videoBox = {
	init: function() {

		this.mudaVideo('lego');
		var $botoes = $('.btns-videos button');

		$botoes.on('click', function(){
			var $this = $(this),
				thisVid = $this.attr('data-video');

			videoBox.botaoClick(thisVid, $botoes, $(this))
		})
	},

	botaoClick: function(videoName, outros, elemento) {
		this.mudaVideo(videoName);
		outros.attr('disabled', false);
		elemento.attr('disabled', true);
	},

	mudaVideo: function(videoName) {
		$('aside.overlay').fadeIn(200);
		setTimeout(function(){
			$('#myBlock').vide({
		  		mp4: 'files/video/' + videoName
			}, {
				loop: true, 
				muted: true, 
			 	autoplay: true,
			 	posterType: "none",
				position: "50% 50%"
			});

			$('#videoTxt').html(
				'<h2>' + videoTextos[videoName]["titulo"] + '</h2>' +
				'<h3>' + videoTextos[videoName]["subTitulo"] + '</h3>' +
				'<p>' + videoTextos[videoName]["texto"] + '</p>' 
			);

		}, 250)

		$('aside.overlay').delay(300).fadeOut();

	}

	// mudaTexto: function()

}

videoBox.init();